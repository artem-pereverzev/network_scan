### --- Prot Scanner -- ###
### --- made Andrew Tymoshchuk for Er-Telecom (Lukoil) --- ###
### --- november 2020 --- ###

import telnetlib
import socket
import time
import ipaddress


# --- Объявляем переменные, создаём пустые списки
subnet = ipaddress.ip_network= input("Введите подсеть для проверки (например:192.168.1.0/24): ") # Приглашение для ввода нужной подсети
username = input("Введите пользователя: ") # Ввод пользователя для подключения
password = input("Введите пароль: ") # Ввод пароля для подключения
port = input("Введите порт для подключения: ") # Приглашение для ввода нужного порта
open_ip = [] # Список для открытых или доступных адресов
closed_ip = [] # Список для закрытых или недоступных адресов
microtik_device = [] # Список для микротиков
cisco_router_device = [] # Список для циски роутеров
cisco_switch_device = [] # Список для циски коммутаторов
zelax_device = [] # Список для зелакса
motorola_device = [] # Список для моторолы
cambium_device = [] # Список для камбиумов
wanflex_device = [] # Список для ванфлекс
bad_ip = [] # Список для проблемных айпи
unknown_device = [] # Список для неизвестных устройств
ipb_1 = [] # Список для ИБП
ban_ip = input("Введите айпи адреса для исключения из проверки(например: 192.168.1.1 192.168.1.2) или нажмите Enter: ") # Список исключений для айпи

cisco_switch_command = input("Введите команду для коммутатора Циско: ")
cisco_enable_mod = input("Нужен режим enable? В большинстве, коммутатор уже в этом режиме. Но не всегда(yes или no): ")
cisco_configt_mod = input("Нужен configure terminal?(yes или no): ")
cisco_write = input("Сделать write после команды?(yes или no): ")
cisco_switch_command_result = []


# --- end ---


for x in subnet.hosts(): # Проходимся циклом по каждому айпи адресу из введённой подсети
    x = str(x)
    if x in ban_ip:
        print(str(x) + " исключён из проверки")
    else:
        ip = str(x)
        retry = 1
        delay = 0.1
        timeout = 0.3
        def isOpen(ip, port):
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.settimeout(timeout)
                print("Сейчас проверяется: " + ip)
                try:
                        s.connect((ip, int(port)))
                        s.shutdown(socket.SHUT_RDWR)
                        return True
                except:
                        return False
                finally:
                        s.close()

        def checkHost(ip, port):
                ipup = False
                for i in range(retry):
                        if isOpen(ip, port):
                                ipup = True
                                break
                        else:
                                time.sleep(delay)
                return ipup
        if checkHost(ip, port): # Если адрес доступен, подключаемся по телнету и считываем приглашение.
            try:
                open_ip.append(str(ip)) # Добавление в список доступных айпи
                print(ip + " открыт и доступен")
                telnet = telnetlib.Telnet(ip) # Подключаемся к айпи адресу
                read_invite = telnet.read_until(b':;', timeout=1) # Сюда записывается приглашение консоли
                telnet.close() # Мягонько закрываем подключение
                if 'Motorola' in str(read_invite): # Ищем "волшебное слово" в приглашении и по нему определям тип устроства + добавление в список
                    print(ip + " это motorola")
                    motorola_device.append(ip)
                else:
                        if 'MikroTik' in str(read_invite):
                                print(ip + " это microtik")
                                microtik_device.append(ip)
                        else:
                                if 'Cambium' in str(read_invite):
                                        print(ip + " это cambium")
                                        cambium_device.append(ip)
                                else:
                                        if  '-sw' in str(read_invite):
                                                print(ip + " это коммутатор cisco")
                                                if len(cisco_switch_command) > 5:
                                                    print("Выполняю команду: " + cisco_switch_command + " на коммутаторе: " + ip)
                                                    telnet_cisco = telnetlib.Telnet(ip)
                                                    telnet_cisco.read_until(b"username: ", timeout=1)
                                                    telnet_cisco.read_until(b"Username: ", timeout=1)
                                                    telnet_cisco.write(username.encode('ascii') + b"\n")
                                                    telnet_cisco.read_until(b"password: ", timeout=1)
                                                    telnet_cisco.read_until(b"Password: ", timeout=1)
                                                    telnet_cisco.write(password.encode('ascii') + b"\n")
                                                    telnet_cisco.write(b"enable\n \n")
                                                    if 'yes' in cisco_configt_mod:
                                                        telnet_cisco.write(b"configure terminal\n \n")
                                                    telnet_cisco.write(cisco_switch_command.encode('ascii') + b"\n   ")
                                                    if 'yes' in cisco_configt_mod:
                                                        telnet_cisco.write(b"exit\n \n")
                                                    if 'yes' in cisco_write:
                                                        telnet_cisco.write(b"write\n \n")
                                                    telnet_cisco.write(b"exit\n")
                                                    cisco_switch_command_result.append(str('-----') + '\n' + ip + '\n' + telnet_cisco.read_all().decode('ascii').strip()+
                                                                                       '\n' + str('-----'))
                                                cisco_switch_device.append(ip)
                                        else:
                                                if 'username: ' in str(read_invite):
                                                    print(ip + " это коммутатор cisco")
                                                    if len(cisco_switch_command) > 5:
                                                        print("Выполняю команду: " + cisco_switch_command + " на коммутаторе: " + ip)
                                                        telnet_cisco = telnetlib.Telnet(ip)
                                                        telnet_cisco.read_until(b"username: ")
                                                        telnet_cisco.write(username.encode('ascii') + b"\n")
                                                        telnet_cisco.read_until(b"password: ")
                                                        telnet_cisco.write(password.encode('ascii') + b"\n")
                                                        telnet_cisco.write(b"enable\n \n")
                                                        if 'yes' in cisco_configt_mod:
                                                            telnet_cisco.write(b"configure terminal\n \n")
                                                        telnet_cisco.write(cisco_switch_command.encode('ascii') + b"\n   ")
                                                        if 'yes' in cisco_configt_mod:
                                                            telnet_cisco.write(b"exit\n \n")
                                                        if 'yes' in cisco_write:
                                                            telnet_cisco.write(b"write\n \n")
                                                        telnet_cisco.write(b"exit\n")
                                                        cisco_switch_command_result.append(str('-----') + '\n' + ip + '\n' + telnet_cisco.read_all().decode('ascii').strip()+ '\n' + str('-----'))
                                                    cisco_switch_device.append(ip)
                                                else:
                                                        if 'WANFleX' in str(read_invite):
                                                                print(ip + " это WANFleX")
                                                                wanflex_device.append(ip)
                                                        else:
                                                                if 'Zelax' in str(read_invite):
                                                                        print(ip + " это Zelax")
                                                                        zelax_device.append(ip)
                                                                else:
                                                                        if 'User Name :' in str(read_invite):
                                                                                print(ip + " это ИБП")
                                                                                ipb_1.append(ip)
                                                                        else:
                                                                                if 'Username: ' in str(read_invite):
                                                                                        print(ip + " это роутер cisco")
                                                                                        cisco_router_device.append(ip)
                                                                                else:
                                                                                        print(ip + " непонятное устройство")
                                                                                        unknown_device.append(ip)
            except ConnectionRefusedError: # Обрабатываем ошибки подключения.
                print(ip + " что-то не так с этим адресом (Connection Refused)")
                bad_ip.append(ip) # Добавляем в список проблемных адресов
                continue
            except EOFError: # Обрабатываем ошибки подключения.
                print(ip + " что-то не так с этим адресом (EOF Error)")
                bad_ip.append(ip) # Добавляем в список проблемных адресов
                continue

        else: # Добавление недоступных адресов в список (на всякий случай сделал)
                closed_ip.append(str(ip + ":" + port))


# Печатаем результаты
print("Микротиков всего: " + str(len(microtik_device)) + '\n' + '\n'.join(microtik_device) + '\n')
print("Моторол всего: " + str(len(motorola_device)) + '\n' + '\n'.join(motorola_device) + '\n')
print("Камбиумов всего: " + str(len(cambium_device)) + '\n' + '\n'.join(cambium_device) + '\n')
print("Цисок коммутаторов всего: " + str(len(cisco_switch_device)) + '\n' + '\n'.join(cisco_switch_device) + '\n')
print("Цисок роутеров всего: " + str(len(cisco_router_device)) + '\n' + '\n'.join(cisco_router_device) + '\n')
print("Ванфлекс всего: " + str(len(wanflex_device)) + '\n' + '\n'.join(wanflex_device) + '\n')
print("Зелаксов всего: " + str(len(zelax_device)) + '\n' + '\n'.join(zelax_device) + '\n')
print("ИБП: " + str(len(ipb_1)) + '\n' + '\n'.join(ipb_1) + '\n')
print("Проблемные адреса: " + str(len(bad_ip)) + '\n' + '\n'.join(bad_ip) + '\n')
print("Неизвестные устройства: " + str(len(unknown_device)) + '\n' + '\n'.join(unknown_device) + '\n')
print("Результат выполнения команды для коммутаторов Циско: " + '\n' +'\n'.join(cisco_switch_command_result) +'\n')


# После завершения работы скрипта
exit_question = input("Скрипт закончил своё дело. Введите exit для выхода или save для сохранения результатов в txt файле: ")
if exit_question == 'exit':
    print("Чао-како!")
    exit()
else:
    if exit_question == 'save':
        results = open('results.txt', 'w')
        results.write("Микротиков всего: " + str(len(microtik_device)) + '\n' + '\n'.join(microtik_device) + '\n' + '\n'
                      "Моторол всего: " + str(len(motorola_device)) + '\n' + '\n'.join(motorola_device) + '\n' + '\n'
                      "Камбиумов всего: " + str(len(cambium_device)) + '\n' + '\n'.join(cambium_device) + '\n' + '\n'
                      "Цисок коммутаторов всего: " + str(len(cisco_switch_device)) + '\n' + '\n'.join(cisco_switch_device) + '\n' + '\n'
                      "Цисок роутеров всего: " + str(len(cisco_router_device)) + '\n' + '\n'.join(cisco_router_device) + '\n' + '\n'
                      "Ванфлекс всего: " + str(len(wanflex_device)) + '\n' + '\n'.join(wanflex_device) + '\n' + '\n'
                      "Зелаксов всего: " + str(len(zelax_device)) + '\n' + '\n'.join(zelax_device) + '\n' + '\n'
                      "ИБП: " + str(len(ipb_1)) + '\n' + '\n'.join(ipb_1) + '\n' + '\n'
                      "Проблемные адреса: " + str(len(bad_ip)) + '\n' + '\n'.join(bad_ip) + '\n' + '\n'
                      "Неизвестные устройства: " + str(len(unknown_device)) + '\n' + '\n'.join(unknown_device) + '\n' + '\n'
                      "Результат выполнения команды для коммутаторов Циско: " + '\n' +  '\n'.join(cisco_switch_command_result))
        results.close()
        print("Сохранение результатов")


